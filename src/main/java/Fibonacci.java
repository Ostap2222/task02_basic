import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

class Fibonacci {
/**
* @param numbersOfFibonacci is ArrayList(generic closed type) which contains numbers of Fibonacci
 */
    private ArrayList<Integer> numbersOfFibonacci = new ArrayList<>();
/**
* Constructor
 */
    Fibonacci() {
        fillNumbersOfFibonacci();
        System.out.println("\nВаші цифри фібоначі:\n"
                + numbersOfFibonacci);

        ArrayList<Integer> fibonacciOddFromStartToEnd = fibonacciOddFromStartToEnd(numbersOfFibonacci);
        System.out.println("Ваші непарні числа від початку до кінця\n" + fibonacciOddFromStartToEnd);

        ArrayList<Integer> fibonacciEvenFromEndToStart = fibonacciEvenFromEndToStart(numbersOfFibonacci);
        System.out.println("Ваші парні числа Фібоначчі від кінця до початку:\n" + fibonacciEvenFromEndToStart);

        printSumArrayList(numbersOfFibonacci);

        ArrayList<Integer> fibonacciLineOfUser = printLineOfFibonacciWithData(fibonacciOddFromStartToEnd, fibonacciEvenFromEndToStart);

        System.out.println("Ваші елементи:" + fibonacciLineOfUser + "\n");

        printPercentOddAndEven(numbersOfFibonacci.size(), fibonacciOddFromStartToEnd, fibonacciEvenFromEndToStart);
    }
/*
* @method that scans the interval from the user's keyboard
*
 */
    private void fillNumbersOfFibonacci() {
        Scanner scanner = new Scanner(System.in);
        int start;
        int end;
        while (true) {
            try {
                System.out.print("Введіть початок інтервалу: ");
                start = scanner.nextInt();
                if (start >= 0) {
                    break;
                } else {
                    System.out.println("\nПочаткове число повинно бути більше ніж " + start);
                }
            } catch (InputMismatchException e) {
                System.out.println("\nНе правильно,введіть знову\n");
                scanner = new Scanner(System.in);
            }
        }
        while (true) {
            try {
                System.out.print("Введіть кінець інтервалу: ");
                end = scanner.nextInt();
                if (end > start) {
                    break;
                } else {
                    System.out.println("\nКінець інтервалу повинен бути більшим ніж початок(" + start + ")"
                            + "\nБудь-ласка, спробуйте знову\n");
                }
            } catch (InputMismatchException e) {
                System.out.println("\nПомилка,спробуйте знову\n");
                scanner = new Scanner(System.in);
            }
        }

        numbersOfFibonacci.add(start);
        numbersOfFibonacci.add(++start);

        fillArrayNumbersOfFibonacciForEndTask(numbersOfFibonacci, end);
    }
/**
* @method compute odd numbers of Fibonacci
* @param arraysOfFibonacci (generic closed type) which contains line of numbers Fibonacci
* @return Fibonacci arrays (with odd numbers) from start to end
 */
    private ArrayList<Integer> fibonacciOddFromStartToEnd(final ArrayList<Integer> arraysOfFibonacci) {
        ArrayList<Integer> fibonacciOddFromStartToEnd = new ArrayList<Integer>();

        for (int i = 0; i < arraysOfFibonacci.size(); i++) {
            if (arraysOfFibonacci.get(i) % 2 == 1) {
                fibonacciOddFromStartToEnd.add(arraysOfFibonacci.get(i));
            }
        }
        return fibonacciOddFromStartToEnd;
    }

    /**
     * @method compute even numbers of Fibonacci
     * @param arraysOfFibonacci contains line of numbers Fibonacci
     * @return Fibonacci arrays (with even numbers) from end to start
     */
    private ArrayList<Integer> fibonacciEvenFromEndToStart(final ArrayList<Integer> arraysOfFibonacci) {
        ArrayList<Integer> fibonacciEvenFromEndToStart = new ArrayList<Integer>();

        Collections.reverse(arraysOfFibonacci);
        for (int i = 0; i < arraysOfFibonacci.size(); i++) {
            if (arraysOfFibonacci.get(i) % 2 == 0) {
                fibonacciEvenFromEndToStart.add(arraysOfFibonacci.get(i));
            }
        }

        return fibonacciEvenFromEndToStart;
    }

    /**
     *
     * @method compute sum of ArrayList
     * @param integers contains data of ArrayList
     */
    private void printSumArrayList(final ArrayList<Integer> integers) {
        int sumOfArrayList = 0;

        for (Integer integer : integers) {
            sumOfArrayList += integer;
        }

        System.out.println("Сума вашого ArrayList = " + sumOfArrayList);
    }

    /**
     *
     * @method print line of Fibonacci by range
     * @param fibonacciOddFromStartToEnd  ArrayList with odd numbers
     * @param fibonacciEvenFromEndToStart ArrayList with even numbers
     * @return ArrayList with data of numbers Fibonacci,
     * where size of ArrayList enter user
     */
    private ArrayList<Integer> printLineOfFibonacciWithData(final ArrayList<Integer> fibonacciOddFromStartToEnd,
                                                            final ArrayList<Integer> fibonacciEvenFromEndToStart) {
        Scanner scanner = new Scanner(System.in);
        int lengthOfLine;
        ArrayList<Integer> lineOfFibonacci = new ArrayList<Integer>();

        while (true) {
            try {
                System.out.print("Введіть кількість елементів фібоначі яку ви хочете: ");
                lengthOfLine = scanner.nextInt();
                if (lengthOfLine > 2) {
                    break;
                } else {
                    System.out.println("\nк-сть елементів фібоначі має бути більше ніж " + lengthOfLine);
                }
            } catch (InputMismatchException e) {
                System.out.println("\nПомилка. Спробуйте знову.\n");
                scanner = new Scanner(System.in);
            }
        }

        lineOfFibonacci.add(Collections.max(fibonacciOddFromStartToEnd));
        lineOfFibonacci.add(Collections.max(fibonacciEvenFromEndToStart));

        int length = lengthOfLine - lineOfFibonacci.size();
        fillArrayNumbersOfFibonacci(lineOfFibonacci, length);

        return lineOfFibonacci;
    }

    /**
     *
     * @method fill ArrayList by numbers of Fibonacci by length
     * @param arraysOfFibonacci buffer for numbers of Fibonacci
     * @param length for compute numbers of Fibonacci
     */
    private void fillArrayNumbersOfFibonacci(final ArrayList<Integer> arraysOfFibonacci, final int length) {
        int start = arraysOfFibonacci.get(1);

        for (int i = 0; i < length; i++) { //compute numbers of fibonacci by range
            start += arraysOfFibonacci.get(i);
            arraysOfFibonacci.add(start);
        }
    }

    /**
     *  @method fill ArrayList by numbers of Fibonacci
     *  @param arraysOfFibonacci buffer for numbers of Fibonacci
     *  @param maxLength         max length of numbers
     *  @method fill ArrayList by numbers of Fibonacci by range
     */
    private void fillArrayNumbersOfFibonacciForEndTask(final ArrayList<Integer> arraysOfFibonacci, final int maxLength) {
        int start = arraysOfFibonacci.get(1);
        int count = 0;

        while (true) {
            start += arraysOfFibonacci.get(count);
            if (start > maxLength) {
                break;
            }
            arraysOfFibonacci.add(start);
            count++;
        }
    }

    /**
     * @method print percent odd and even numbers of numbers of Fibonacci
     * @param sizeAllNumbers int for formula
     * @param oddNumbers     all odd numbers
     * @param evenNumbers    all even numbers
     */
    private void printPercentOddAndEven(final int sizeAllNumbers, final ArrayList<Integer> oddNumbers, final ArrayList<Integer> evenNumbers) {
        final int percent = 100;

        double percentOfOdd = (double) oddNumbers.size() / (double) sizeAllNumbers * percent;
        double percentOfEven = (double) evenNumbers.size() / (double) sizeAllNumbers * percent;

        percentOfOdd = (double) Math.round(percentOfOdd * percent) / percent;
        percentOfEven = (double) Math.round(percentOfEven * percent) / percent;
        System.out.println("Процент додатніх чисел = " + percentOfOdd + "%");
        System.out.println("Процент відємних чисел = " + percentOfEven + "%");
    }
}